# coding: utf-8
from os.path import join, abspath, dirname
VERSION = '2019.03-20'
PROJECT_ROOT = join(abspath(dirname(__file__)), '..')
def root_join(*path):
   return join(abspath(PROJECT_ROOT), *path)