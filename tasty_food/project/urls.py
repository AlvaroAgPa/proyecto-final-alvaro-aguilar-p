from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls), 
    path('web/', include('web.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Tasty Food / T-mob"
admin.site.site_title = "Tasty Food Admin"
admin.site.index_title = "Welcome to Tasty Food"



