# Generated by Django 2.2.5 on 2019-10-17 17:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_auto_20191017_1722'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='image_recipe',
            options={'verbose_name_plural': 'Images Recipe'},
        ),
        migrations.AlterModelOptions(
            name='ingredient',
            options={'verbose_name_plural': 'Ingredients'},
        ),
        migrations.AlterModelOptions(
            name='recipe',
            options={'verbose_name_plural': 'Recipes'},
        ),
        migrations.AlterModelOptions(
            name='step',
            options={'verbose_name_plural': 'Steps'},
        ),
    ]
