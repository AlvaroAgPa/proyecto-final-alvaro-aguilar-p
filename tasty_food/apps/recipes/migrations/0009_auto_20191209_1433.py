# Generated by Django 2.2.5 on 2019-12-09 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_auto_20191205_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='serving',
            field=models.IntegerField(),
        ),
    ]
