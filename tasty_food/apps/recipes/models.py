# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _
from django import forms
from categories.models import Category
from chefs.models import Chef


class Recipe(models.Model):

    class Meta:
        verbose_name = ('recipe')
        verbose_name_plural = ('recipes')
        db_table = 'tasty_food_recipes'

    CHOICE_TIME_DURATION = (('5', '5 Minutos'), 
                            ('10', '10 Minutos'),
                            ('15', '15 Minutos'),
                            ('20', '20 Minutos'),
                            ('25', '25 Minutos'),
                            ('30', '30 Minutos'),
                            ('35', '35 Minutos'),
                            ('40', '40 Minutos'),
                            ('45', '45 Minutos'),
                            ('50', '50 Minutos'),
                            ('55', '55 Minutos'),
                            ('60', '1hr'),
                            ('65', '1hr 5 Minutos'),
                            ('70', '1hr 10 Minutos'),
                            ('75', '1hr 15 Minutos'),
                            ('80', '1hr 20 Minutos'),
                            ('90', '1hr 30Minutos'),
                            )
    CHOICE_LEVEL = (('1', 'Facil'),
                    ('2', 'Medio'),
                    ('3', 'Dificil'),
                    )
    
    name = models.CharField(_("name"), max_length=50)
    chef = models.ForeignKey(Chef, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    active = models.BooleanField("active", default=True)

    video = models.URLField(_("video URL"),max_length=200, null=True, blank=True)
    spotify = models.URLField(_("spotify URL"),max_length=200, null=True, blank=True)
    #image = models.ImageField()

    duration = models.CharField(_("duration"), choices=CHOICE_TIME_DURATION, max_length=5)
    level = models.CharField(_("level"),choices=CHOICE_LEVEL, max_length=15)
    serving = models.IntegerField(_("serving"), )

    updated_at = models.DateTimeField(auto_now=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__ (self):
        return self.name
    
    def preview(self):
        #if self.video:
        #    return self.video
        return self.images.all().order_by('order')[0].image.url

class Ingredient(models.Model):

    class Meta:
        verbose_name = _('ingredient')
        verbose_name_plural = _('ingredients')
        db_table = 'tasty_food_ingredients'

    name = models.CharField(max_length=50)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='ingredients')

    def __str__ (self):
        return self.name


class Step(models.Model):

    class Meta:
        verbose_name = _('step')
        verbose_name_plural = _('steps')
        db_table = 'tasty_food_steps'

    order = models.PositiveSmallIntegerField()
    process = models.TextField("Proceso")
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='steps')

    def __str__ (self):
        return self.process

class ImageRecipe(models.Model):

    class Meta:
        verbose_name = _('image recipe')
        verbose_name_plural = _('images recipe')
        db_table = 'tasty_food_images_recipe'

    order = models.PositiveSmallIntegerField()
    image = models.ImageField(upload_to='recipe')
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='images')

