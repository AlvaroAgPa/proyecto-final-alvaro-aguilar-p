from django.contrib import admin
from django.db import models
from .models import Recipe, Ingredient, Step, ImageRecipe
from django.forms import Textarea, TextInput


class IngredientInLine(admin.TabularInline):
    model = Ingredient 
    min_num = 1
    extra = 0
    classes = ("collapse", )
    formfield_overrides = {
       models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':90})},
       models.CharField: {'widget': TextInput(attrs={'size':160})},
    }

class StepInLine(admin.TabularInline):
    model = Step
    min_num = 1
    extra = 0
    classes = ("collapse", )
    formfield_overrides = {
       models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':90})},
       models.CharField: {'widget': TextInput(attrs={'size':160})},
    }
    
    fieldsets = (
          (None, { 'fields': ('order', 'process', ) }),
    )
class ImageRecipeInLine(admin.TabularInline):
    model = ImageRecipe
    min_num = 1
    extra = 0
    classes = ("collapse", )
    formfield_overrides = {
       models.CharField: {'widget': TextInput(attrs={'size':160})},
    }

@admin.register(Recipe)
class AdminRecipe(admin.ModelAdmin):
    inlines = [IngredientInLine, StepInLine, ImageRecipeInLine]

    formfield_overrides = {
       models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':90})},
       models.CharField: {'widget': TextInput(attrs={'size':50})},
       models.URLField: {'widget': TextInput(attrs={'size':100})},
    }
    list_filter = ('category', 'chef', 'active')
    list_display = ('name', 'category', 'chef', 'duration', 'level', 'serving', 'active', 'updated_at', 'created_at')
    
    fieldsets = (
          (None, { 'fields': ('name', 'category', 'chef', 'active', ) }),
          ('info', { 'fields': (('duration', 'level', 'serving'), ) }),
          ('media', { 'fields': ('video', 'spotify') }),
          #('Other apps', { 'fields': (, ) }),
          #('Info recipes', { 'fields': ('serving', 'level', 'duration', ) }),
    )

# @admin.register(Ingredient)
# class AdminIngredient(admin.ModelAdmin):
#     list_display = ('name', )
#
# @admin.register(Step)
# class AdminSteps(admin.ModelAdmin):
#     list_display = ('process', )