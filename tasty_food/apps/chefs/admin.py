# coding: utf-8

from django.contrib import admin
from .models import Chef
from django.db import models
from django.forms import Textarea, TextInput

@admin.register(Chef)
class AdminChef(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':70})},
        models.TextField: {'widget': Textarea(attrs={'rows':20, 'cols':120})},
    }
    list_display = ('name', 'biography')
