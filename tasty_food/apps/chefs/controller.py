from .models import Chef
from django.core.cache import cache
from django.conf import settings
from settings.local import CACHE_CHEF

def reload_cache_chef():

    chefs = Chef.objects.filter(active=True)
    chef_cache = {chef.id:chef for chef in chefs }
    cache.set(settings.CACHE_CATEGORY, chef_cache)
    return chef_cache



def get_chef(chef_id):
    
    chef_cache = cache.get(settings.CACHE_CHEF) 
    if not chef_cache:                               
        chef_cache = reload_cache_chef() 

    try:
        chef = chef_cache[chef_id]
        return chef
    except:
        return None



def get_chefs():

    chef_cache = cache.get(settings.CACHE_CHEF) 
    if not chef_cache:                               
        chef_cache = reload_cache_chef()
    
    chefs = list(chef_cache.values())
    chefs.sort(key=lambda x: x.name)
    return chefs