# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _
from categories.validate import validate_svg


class Chef(models.Model):

    class Meta:
        verbose_name = _('chef')
        verbose_name_plural = _('chefs')
        db_table = 'tasty_food_chefs'

    name = models.CharField(_("name"), max_length=50)
    image = models.FileField(_("banner"), upload_to='chefs', validators=[validate_svg])
    biography = models.TextField(_("biography"))
    active = models.BooleanField(default=True)

    def __str__ (self):
        return self.name

    def preview(self):
        return self.image.url