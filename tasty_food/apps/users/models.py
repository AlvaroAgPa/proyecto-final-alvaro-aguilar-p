from django.db import models
from recipes.models import Recipe

class User(models.Model):
    number = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    service_id = models.IntegerField(blank=False)

    def __str__ (self):
        return self.number

class Favorites(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    fav_recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)

    def __str__ (self):
        return self.user