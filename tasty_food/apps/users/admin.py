from django.contrib import admin
from .models import User, Favorites


@admin.register(User)
class AdminUser(admin.ModelAdmin):
    list_display = ('number', 'name', 'active', )

@admin.register(Favorites)
class AdminFavorites(admin.ModelAdmin):
    list_display = ('user', 'fav_recipe')
