from .models import Showcase
from django.core.cache import cache
from django.conf import settings
from settings.local import CACHE_SHOWCASE

def reload_cache_showcase():

    showcases = Showcase.objects.filter(active=True)
    showcase_cache = {showcase.id:showcase for showcase in showcases }
    cache.set(settings.CACHE_SHOWCASE, showcase_cache)
    return showcase_cache



def get_showcase(showcase_id):
    
    showcase_cache = cache.get(settings.CACHE_SHOWCASE) 
    if not showcase_cache:                               
        showcase_cache = reload_cache_showcase() 

    try:
        showcase = showcase_cache[showcase_id]
        return showcase
    except:
        return None



def get_showcase():

    showcase_cache = cache.get(settings.CACHE_SHOWCASE) 
    if not showcase_cache:                               
        showcase_cache = reload_cache_showcase()