from django.contrib import admin
from django.db import models
from .models import Showcase, RecipeInShowcase, ChefInShowcase
from django.forms import Textarea, TextInput

class RecipeInShowcaseInLine(admin.StackedInline):
    model = RecipeInShowcase 
    min_num = 1
    extra = 0
    classes = ("collapse", )
    formfield_overrides = {
       models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':90})},
       models.CharField: {'widget': TextInput(attrs={'size':10})},
    }

class ChefInShowcaseInLine(admin.StackedInline):
    model = ChefInShowcase
    min_num = 1
    extra = 0
    classes = ("collapse", )
    formfield_overrides = {
       models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':90})},
       models.CharField: {'widget': TextInput(attrs={'size':10})},
    }

@admin.register(Showcase)
class ShowcaseAdmin(admin.ModelAdmin):
    inlines = [RecipeInShowcaseInLine, ChefInShowcaseInLine,]
    list_display = ('name', 'order')

