from django.apps import AppConfig


class AppShowcaseConfig(AppConfig):
    name = 'app_showcase'
