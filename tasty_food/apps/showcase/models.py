# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _
from recipes.models import Recipe
from chefs.models import Chef

class Showcase(models.Model):
    class Meta:
        verbose_name = _('showcase')
        verbose_name_plural = _('showcases')
        db_table = 'tasty_food_showcase'

    name = models.CharField(_('name'), max_length=50)
    order = models.PositiveSmallIntegerField(_('order'), default=0, help_text="Indica el orden en que se muestran los diferentes showcases.")
    active = models.BooleanField(_('active'), default=True)

    def __str__ (self):
        return "{} [{}]".format(self.name, self.order)

class RecipeInShowcase(models.Model):
    class Meta:
        verbose_name = _('recipe in showcase')
        verbose_name_plural = _('recipes in showcase')
        db_table = 'tasty_food_showcase_recipe'

    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='showcase')
    showcase = models.ForeignKey(Showcase, on_delete=models.CASCADE, related_name='recipes')


class ChefInShowcase(models.Model):
    class Meta:
        verbose_name = _('chef in showcase')
        verbose_name_plural = _('chefs in showcase')
        db_table = 'tasty_food_showcase_chef'

    chef = models.ForeignKey(Chef, on_delete=models.CASCADE, related_name='showcase')
    showcase = models.ForeignKey(Showcase, on_delete=models.CASCADE, related_name='chef')
