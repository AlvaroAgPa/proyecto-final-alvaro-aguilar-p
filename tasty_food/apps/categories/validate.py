import sys
from io import BytesIO
from PIL import Image
import xml.etree.cElementTree as et

from django.core.exceptions import ValidationError
from django.forms import ImageField as DjangoImageField
from django.utils import six


class SVGAndImageFormField(DjangoImageField):

    def to_python(self, data):
        
        test_file = super(DjangoImageField, self).to_python(data)
        if test_file is None:
            return None

            ifile = data.temporary_file_path()
        else:
            if hasattr(data, 'read'):
                ifile = BytesIO(data.read())
            else:
                ifile = BytesIO(data['content'])

        try:
            image = Image.open(ifile)
            image.verify()

            test_file.image = image
            test_file.content_type = Image.MIME[image.format]
        except Exception:

            if not self.is_svg(ifile):
                six.reraise(ValidationError, ValidationError(
                    self.error_messages['invalid_image'],
                    code='invalid_image',
                ), sys.exc_info()[2])
        if hasattr(test_file, 'seek') and callable(test_file.seek):
            test_file.seek(0)
        return test_file

    def is_svg(self, f):

        f.seek(0)
        tag = None
        try:
            for event, el in et.iterparse(f, ('start',)):
                tag = el.tag
                break
        except et.ParseError:
            pass
        return tag == '{http://www.w3.org/2000/svg}svg'

#VALIDATORS

def validate_svg(file):
    _svg = SVGAndImageFormField().is_svg(file)
    if not _svg:
        #raise ValidationError("File not svg")
        return None
    return file