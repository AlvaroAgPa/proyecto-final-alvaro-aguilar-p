# coding: utf-8

from django.contrib import admin
from .models import Category
from django.db import models
from django.forms import TextInput, Textarea, SelectMultiple

@admin.register(Category)
class AdminCategory(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':70})},
        models.URLField: {'widget': TextInput(attrs={'size':70})},
    }

    list_display = ('name', 'active')

