from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .models import Category
from .controller import reload_cache_category

@receiver(post_save, sender=Category)
def new_category(sender, instance, *args, **kwargs):
    reload_cache_category()