# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _
from .validate import validate_svg

class Category(models.Model):

    class Meta:
        verbose_name_plural = ('Categories')
        db_table = 'tasty_food_categories'

    name = models.CharField(_('name'), max_length=50)
    icon = models.FileField(_('icon'), upload_to='icons', validators=[validate_svg], help_text="Icono de la categoria mostrado en el listado de las mismas.")
    banner = models.FileField(_('banner'), upload_to='banners', validators=[validate_svg], help_text="Banner de la categoria mostrado en la descripcion de la misma.")
    active = models.BooleanField(_('active'), default=True)

    def __str__ (self):
        return self.name