# Generated by Django 2.2.5 on 2019-12-18 19:12

import categories.validate
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '0008_auto_20191114_1948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='banner',
            field=models.FileField(help_text='Banner de la categoria mostrado en la descripcion de la misma.', upload_to='banners', validators=[categories.validate.validate_svg], verbose_name='banner'),
        ),
        migrations.AlterField(
            model_name='category',
            name='icon',
            field=models.FileField(help_text='Icono de la categoria mostrado en el listado de las mismas.', upload_to='icons', validators=[categories.validate.validate_svg], verbose_name='icon'),
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=50, verbose_name='name'),
        ),
    ]
