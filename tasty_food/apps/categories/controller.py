from .models import Category
from django.core.cache import cache
from django.conf import settings
from settings.local import CACHE_CATEGORY

def reload_cache_category():

    categories = Category.objects.filter(active=True)
    cat_cache = {category.id:category for category in categories }
    cache.set(settings.CACHE_CATEGORY, cat_cache)
    return cat_cache



def get_category(category_id):
    
    cat_cache = cache.get(settings.CACHE_CATEGORY) 
    if not cat_cache:                               
        cat_cache = reload_cache_category() 

    try:
        category = cat_cache[category_id]
        return category
    except:
        return None



def get_categories():

    cat_cache = cache.get(settings.CACHE_CATEGORY) 
    if not cat_cache:                               
        cat_cache = reload_cache_category()
    
    categories = list(cat_cache.values())
    categories.sort(key=lambda x: x.name)
    return categories