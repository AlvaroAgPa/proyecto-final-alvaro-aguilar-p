from django.shortcuts import render
from categories.controller import get_categories, get_category
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from recipes.models import Recipe
from chefs.models import Chef
from users.models import User
from showcase.models import Showcase
from datetime import datetime, timedelta

def subscription_active(hs):
    return True

def login(request):
    param = {'title' : 'TASTY FOOD'}
    return render(request, 'web/login.html', param)

def msisdn(request):
    
    param = {'title' :  'TASTY FOOD'}
    return render(request, 'web/msisdn.html', param)

def pincode(request):
    param = {'title' : 'TASTY FOOD'}
    return render(request, 'wen/pincode.html', param)

def home(request):
   
    hs = request.GET.get('hs', None)
    
    if not hs:
        current_session = request.session.get('session_cookie', None)
        print (current_session)

        if not current_session:
            print('NoSession')
            
            active = False
            request.session['session_cookie'] = datetime.today().strftime("%Y-%d-%m %H:%M:%S")
            request.session['hs_cookie'] = hs
        
        else:
            session_date = datetime.strptime(current_session, "%Y-%d-%m %H:%M:%S")
            hs = request.session.get('hs_cookie', None)
            active = True

            if not session_date >= (datetime.now() - timedelta(days=30)):
                active = subscription_active(hs)
                request.session['session_cookie'] = datetime.today().strftime("%Y-%d-%m %H:%M:%S")
                request.session['hs_cookie'] = hs
    
    else:
        active = subscription_active(hs)
        
    if active:
        request.session['session_cookie'] = datetime.today().strftime("%Y-%d-%m %H:%M:%S")
        request.session['hs_cookie'] = hs
    
        template = 'web/index.html'
    
    else:
        template = 'web/login.html'
    
    showcase = Showcase.objects.filter(active=True)

    param = {'title': 'TASTY FOOD',
             'page': 'home',
             'showcase': showcase,
             'categories' : get_categories(),}

    return render(request, template, param)

def recipe(request, recipe_id):
    recipe = Recipe.objects.get(pk=recipe_id)
    param = {'recipe': recipe,
             'steps': recipe.steps.all().order_by('order'),
             'title': 'TASTY FOOD',
             'page': 'home',
             'categories' : get_categories(),
            }
    return render(request, 'web/recipe.html', param)


def categories(request):
    param = {'categories' : get_categories(), 
             'title' : 'CATEGORIAS', 
             'page' : 'categories'}
    return render(request,'web/categories.html', param)

def category(request, category_id):
    category = get_category(category_id)
    recipes = Recipe.objects.filter(active=True, category=category)
    param = {'category' : category, 'title' : category.name.upper(), 'recipes': recipes, 'total_recipes' : len(recipes), 'page' : 'categories'}
    return render(request,'web/category.html', param)

def chef_detail(request, chef_id):
    chef = Chef.objects.get(pk=chef_id, active=True)
    recipes = Recipe.objects.filter(active=True, chef=chef).order_by('created_at')
    param = {'title': 'NUESTROS CHEFS',
             'page': 'categories',
             'n_recipes': len(recipes),
             'recipes': recipes,
             'chef': chef, 
             'categories' : get_categories(),}
    return render(request, 'web/chef_detail.html', param)

def chef_list(request):
    chefs = Chef.objects.filter(active=True)
    param = {'title': 'NUESTROS CHEFS',
             'page': 'categories',
             'chefs': chefs, 
             'categories' : get_categories(),}
    return render(request, 'web/chef_list.html', param)

def notifications(request):
    param = {'title' : 'NOTIFICACIONES'}
    return render(request, 'web/notifications.html', param)

def favorites(request):
    param = {'title' : 'FAVORITOS', 'page' : 'favorites'}
    return render(request, 'web/favorites.html', param)

@login_required
def user(request):
    param = {'title' : 'USUARIO'}
    return render(request, 'web/user.html', param)

def videos(request):
    recipes = Recipe.objects.exclude(video__isnull=True)
    videos = ["https://player.vimeo.com/video/376894745",
            "https://player.vimeo.com/video/376896747",
            "https://player.vimeo.com/video/376896185",
            "https://player.vimeo.com/video/376895647",
            "https://player.vimeo.com/video/376895354",
            "https://player.vimeo.com/video/376888873",
            "https://player.vimeo.com/video/376894500",
            "https://player.vimeo.com/video/376893997",
            "https://player.vimeo.com/video/376893682",
            "https://player.vimeo.com/video/376892665",
            "https://player.vimeo.com/video/376890650",
            "https://player.vimeo.com/video/376889732",]
    param = {'title': 'VIDEOS',
             'page': 'videos', 
             'recipes': recipes,
             'categories' : get_categories(),
             'videos': videos,}
    return render(request, 'web/videos.html', param)

def search(request):
    print(request.GET)
    category_id = request.POST.get("category", None)
    ingredient = request.POST.get("ingredient", None)
    recipes = None
    if category_id and ingredient:
        recipes = Recipe.objects.filter(active=True, category__id=category_id, ingredients__name__icontains=ingredient)
    else:
        if category_id:
            recipes = Recipe.objects.filter(active=True, category__id=category_id)

    param = {'title': 'TASTY FOOD',
             'page': 'home', 
             'recipes': recipes,
             'categories' : get_categories(),}
    return render(request, 'web/search-results.html', param)
    
    
    
    