from django.urls import path, include
from .views import *


urlpatterns = [ 
    path('login/', login),
    path('home/', home, name='home'),
    path('recipe/<int:recipe_id>/', recipe, name='recipe'),
    path('categories/', categories, name='categories'),
    path('category/<int:category_id>/', category, name='category'),
    path('chef/<int:chef_id>/', chef_detail, name='chef' ),
    path('chef_list/', chef_list, name='chefs'),
    path('notifications/', notifications),
    path('favorites/', favorites, name='favorites'),
    path('user/', user),
    path('videos/', videos, name='videos'),
    path('search/', search, name='search'),
]

