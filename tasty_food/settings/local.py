from settings.base import *

DATABASES = {
   'default': {
       'NAME': 'tasty_food',
       'ENGINE': 'django.db.backends.mysql',
       'USER': 'root',
       'PASSWORD': 'root1234',
   }
}

#CACHE SETTINGS

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

CACHE_CATEGORY = 'cache_category'

CACHE_CHEF = 'cache_chef'

CACHE_SHOWCASE = 'cache_showcase'

